<?php
  /*
  1. Create a function createProduct with params product name and price, and add it to an array called products

  2. Create a function called printProducts that displays
  the list of elements of products in a UL in index.php

  3. Create a function called countProducts, display count
  of elements in product array

  4. Create function deleteProduct that deletes an element in product array
  */

  $products = []; // global var

  function createProduct($name, $price) {
    global $products; // need 'global' keyword to access global var
    echo "Creating product $name... <br/>";
    $products[$name] = $price;
  }

  function printProducts() {
    global $products;
    echo "<h4>Product List:</h4>";
    echo "<ul>";
    
    foreach($products as $name=>$price) {
      echo "<li>$name --- $price</li>";
    }

    echo "</ul>";
  }

  function countProducts() {
    global $products;
    echo "Counting products: ".count($products)."<br/>";
  }

  function deleteProduct() {
    global $products;
    echo "Deleting product... <br/>";
    array_pop($products);
  }
?>