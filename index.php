<?php require 'code.php'; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Array Manipulation | Activity</title>
</head>
<body>
  <h1>Activity</h1>
  <?php 
    createProduct("Cellphone", 12000);
    createProduct("Car", 160000);
    createProduct("Tank", 260000);
    createProduct("Helicopter", 425000);
    createProduct("Giant Death Robot", 1560500);

    printProducts();
    countProducts();
    deleteProduct();
    
    printProducts();
    countProducts();
    deleteProduct();  
    
    printProducts();
  ?>
</body>
</html>